import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm'

export class QuestionChoices1545106905029 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query('')
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    const table = (await queryRunner.getTable('QuestionChoices')) as Table
    const foreignKey = table.foreignKeys.find(
      fk => fk.columnNames.indexOf('questionId') !== -1,
    ) as TableForeignKey
    await queryRunner.dropForeignKey('QuestionChoices', foreignKey)
    await queryRunner.dropColumn('QuestionChoices', 'questionId')
    await queryRunner.dropTable('QuestionChoices')
  }
}
