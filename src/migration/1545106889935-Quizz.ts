import {
  MigrationInterface,
  QueryRunner,
  TableForeignKey,
  Table,
} from 'typeorm'

export class Quizz1545106889935 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query('')
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    const table = (await queryRunner.getTable('Quizz')) as Table
    const foreignKey = table.foreignKeys.find(
      fk => fk.columnNames.indexOf('questionId') !== -1,
    ) as TableForeignKey
    await queryRunner.dropForeignKey('Quizz', foreignKey)
    await queryRunner.dropColumn('Quizz', 'questionId')
    await queryRunner.dropTable('Quizz')
  }
}
