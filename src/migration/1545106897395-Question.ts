import {
  MigrationInterface,
  QueryRunner,
  TableForeignKey,
  Table,
} from 'typeorm'

export class Question1545106897395 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query('')
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    const table = (await queryRunner.getTable('Question')) as Table
    const foreignKey = table.foreignKeys.find(
      fk => fk.columnNames.indexOf('quizzId') !== -1,
    ) as TableForeignKey
    await queryRunner.dropForeignKey('Question', foreignKey)
    await queryRunner.dropColumn('Question', 'quizzId')
    await queryRunner.dropTable('Question')
  }
}
