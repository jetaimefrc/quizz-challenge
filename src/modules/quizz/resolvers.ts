import { getRepository } from 'typeorm'
import { Quizz } from '../../entity/Quizz'

export const resolvers = {
  Query: {
    quizz: async () => {
      const quizzRepo = getRepository(Quizz)
      const quizzs = await quizzRepo.find({
        relations: ['question'],
      })
      return quizzs
    },
  },
}
