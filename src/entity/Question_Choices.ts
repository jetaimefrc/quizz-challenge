import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinColumn,
  ManyToOne,
} from 'typeorm'
import { Question } from './Question'

@Entity()
export class QuestionChoices {
  @PrimaryGeneratedColumn()
  public id: Number

  @Column()
  public isRightChoice: Boolean

  @Column()
  public choice: String

  @JoinColumn()
  @ManyToOne(() => Question, quest => quest.id)
  public question: Question
}
